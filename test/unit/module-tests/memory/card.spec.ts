import { Card } from "../../../../src/modules/memory/models/card";

describe("Card", () => {
    it("has default numeric value 1", () => {
        // Arrange
        const card: Card = new Card();
        const expected: number = 1;
        let result: number;

        // Act
        result = card.value;

        // Assert
        expect(result).toBe(expected);
    });

    it("has is assigned specific numeric value through constructor", () => {
        // Arrange
        const card: Card = new Card(2);
        const expected: number = 2;
        let result: number;

        // Act
        result = card.value;

        // Assert
        expect(result).toBe(expected);
    });
});