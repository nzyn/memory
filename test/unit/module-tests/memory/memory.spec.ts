import { Memory } from "../../../../src/modules/memory/memory";
import { Card } from "modules/memory/models/index";

describe("Memory module", () => {
    it("successfully initializes default array of cards", () => {
        // Arrange
        const module: Memory = new Memory();
        const expected: Card[] = [
            new Card(1), new Card(1),
            new Card(2), new Card(2),
            new Card(3), new Card(3)
        ];

        // Act
        module.initializeCards();

        // Assert
        expect(module.cards).toEqual(expected);
    });

    it("successfully initializes array of cards with specific size", () => {
        // Arrange
        const module: Memory = new Memory();
        const expected: Card[] = [
            new Card(1), new Card(1),
            new Card(2), new Card(2)
        ];

        // Act
        module.initializeCards(2);

        // Assert
        expect(module.cards).toEqual(expected);
    });
});