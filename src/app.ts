import { RouterConfiguration, RouteConfig } from "aurelia-router";
import { PLATFORM } from "aurelia-pal";
import "./sass/main.scss";

export class App {
  router: RouteConfig;

  configureRouter(config: RouterConfiguration, router: RouteConfig) {
    config.title = "Memory";
    config.options.pushState = true;

    config.map([
      { route: "", name: "home", title: "Home", moduleId: PLATFORM.moduleName("modules/home/home"), nav: true },
      { route: "games/memory", name: "memory", title: "Memory", moduleId: PLATFORM.moduleName("modules/memory/memory"), nav: true },
    ]);

    this.router = router;
  }
}
