import { Card } from "modules/memory/models";

export class Memory {
    cards: Card[];

    initializeCards(numberOfPairs: number = 3) {
        this.cards = [];

        for (let index = 0; index < numberOfPairs; index++) {
            this.cards.push(new Card(index + 1), new Card(index + 1));
        }
    }
}